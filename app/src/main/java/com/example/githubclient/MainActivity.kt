package com.example.githubclient

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.githubclient.model.entities.GithubIssue
import com.example.githubclient.model.entities.GithubRepo
import com.example.githubclient.views.dialogs.AddIssueDialog
import com.example.githubclient.views.dialogs.CredentialsDialog
import com.example.githubclient.views.fragments.IssuesListFragment
import com.example.githubclient.views.fragments.ProjectListFragment


class MainActivity : AppCompatActivity(),
    ProjectListFragment.ProjectListEvents,
    CredentialsDialog.ICredentialsDialogListener,
    IssuesListFragment.IssueListEvents,
    AddIssueDialog.AddIssueDialogListener {

    companion object {
        const val USERNAME_KEY = "USERNAME_KEY"
        const val PASSWORD_KEY = "PASSWORD_KEY"
        const val ENDPOINT = "https://api.github.com"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        displayFragmentList()
    }

    private fun displayFragmentList() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, ProjectListFragment(), "ProjectListFragment")
            .addToBackStack("ProjectListFragment")
            .commit()
    }


    override fun showCredentialsDialog() {
        val dialog = CredentialsDialog()
        dialog.show(supportFragmentManager, "credentialsDialog")
    }

    override fun onDialogPositiveClick(username: String, password: String) {
        val fragment = supportFragmentManager.findFragmentByTag("ProjectListFragment") as ProjectListFragment
        fragment.getRepos(username, password)
    }

    override fun showIssueListFragment(repo: GithubRepo) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, IssuesListFragment.newInstance(repo), "IssuesListFragment")
            .addToBackStack("IssuesListFragment")
            .commit()
    }

    override fun showAddIssueDialog(githubRepo: GithubRepo) {
        val dialog = AddIssueDialog()
        dialog.show(supportFragmentManager, "AddIssueDialog")
    }

    override fun addIssue(title: String, comment: String) {
        val fragment = supportFragmentManager.findFragmentByTag("IssuesListFragment") as IssuesListFragment
        fragment.addIssue(title, comment)
    }
}
