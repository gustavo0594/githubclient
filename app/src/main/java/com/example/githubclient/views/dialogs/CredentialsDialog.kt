package com.example.githubclient.views.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.example.githubclient.R


class CredentialsDialog:DialogFragment() {
    internal interface ICredentialsDialogListener {
        fun onDialogPositiveClick(username: String, password: String)
    }

    private lateinit var listener: ICredentialsDialogListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ICredentialsDialogListener) {
            listener = context
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val rootView = activity?.layoutInflater?.inflate(R.layout.credentials_dialog_layout, null)
        val builder = AlertDialog.Builder(context)
                .setView(rootView)
                .setTitle(R.string.credentials)
                .setNegativeButton(R.string.credentials_negative_button, null)
                .setPositiveButton(R.string.credentials_positive_button) { _, _ ->
                    with(rootView) {
                        listener.onDialogPositiveClick(
                                this?.findViewById<EditText>(R.id.edtUsername)?.text.toString(),
                                this?.findViewById<EditText>(R.id.edtPassword)?.text.toString())
                    }
                }


        return builder.create()
    }
}
