package com.example.githubclient.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.R
import com.example.githubclient.model.entities.GithubRepo
import com.example.githubclient.views.viewHolders.RepositoryViewHolder

class RepositoryListAdapter(private var data:List<GithubRepo>, private var listener: RepositoryViewHolder.RepositoryListSelectedListener?) : RecyclerView.Adapter<RepositoryViewHolder>() {

    fun setData(data:List<GithubRepo>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_repos_list_layout, parent, false)
        return RepositoryViewHolder(view, listener)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        holder.bind(data[position])
    }
}