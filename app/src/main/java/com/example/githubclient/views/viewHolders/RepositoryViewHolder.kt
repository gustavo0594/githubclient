package com.example.githubclient.views.viewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.model.entities.GithubRepo
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cell_repos_list_layout.view.*

class RepositoryViewHolder(view: View, private val listener: RepositoryListSelectedListener?) : RecyclerView.ViewHolder(view) {

    fun bind(repo: GithubRepo) {
        with(itemView) {
            ownerNameTextView.text = repo.owner.login
            projectNameTextView.text = repo.name
            projectUrlTextView.text = repo.url
            Picasso.get().load(repo.owner.avatarUrl).into(ownerPicImageView)
            itemView.setOnClickListener { listener?.onRepoSelected(repo) }
        }
    }

    interface RepositoryListSelectedListener {
        fun onRepoSelected(repo: GithubRepo)
    }
}