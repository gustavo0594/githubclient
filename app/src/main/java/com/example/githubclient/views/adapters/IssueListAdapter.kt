package com.example.githubclient.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.R
import com.example.githubclient.model.entities.GithubIssue
import com.example.githubclient.views.viewHolders.IssueViewHolder

class IssueListAdapter(private var data:List<GithubIssue>) : RecyclerView.Adapter<IssueViewHolder>() {

    fun setData(data:List<GithubIssue>) {
        this.data = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IssueViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_issues_list_layout, parent, false)
        return IssueViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: IssueViewHolder, position: Int) {
        holder.bind(data[position])
    }
}