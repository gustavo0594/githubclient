package com.example.githubclient.views.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.example.githubclient.R

class AddIssueDialog : DialogFragment() {

    internal interface AddIssueDialogListener {
        fun addIssue(title: String, comment: String)
    }

    private lateinit var listener: AddIssueDialogListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is AddIssueDialogListener) {
            listener = context
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val rootView = activity?.layoutInflater?.inflate(R.layout.add_issue_dialog_layout, null)
        val builder = AlertDialog.Builder(context)
            .setView(rootView)
            .setTitle(R.string.issue)
            .setNegativeButton(R.string.credentials_negative_button, null)
            .setPositiveButton(R.string.credentials_positive_button) { _, _ ->
                with(rootView) {
                    listener.addIssue(
                        this?.findViewById<EditText>(R.id.edtIssueTitle)?.text.toString(),
                        this?.findViewById<EditText>(R.id.edtIssueComment)?.text.toString()
                    )
                }
            }


        return builder.create()
    }

}
