package com.example.githubclient.views.util

import android.content.Context
import android.content.SharedPreferences

object PreferencesUtil {

    private const val PREFERENCES_KEY = "PREFERENCES_KEY"

    private fun getPreferences(context:Context): SharedPreferences = context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE)

    fun putString(context: Context, key:String, value: String) {
        getPreferences(context).edit().putString(key, value).apply()
    }

    fun getString(context: Context, key: String, default:String? = ""): String {
        return getPreferences(context).getString(key, default)
    }
}