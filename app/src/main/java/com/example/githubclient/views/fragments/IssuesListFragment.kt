package com.example.githubclient.views.fragments

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubclient.MainActivity
import com.example.githubclient.R
import com.example.githubclient.model.entities.GithubIssue
import com.example.githubclient.model.entities.GithubRepo
import com.example.githubclient.model.services.GithubAPI
import com.example.githubclient.model.services.InterceptorApi
import com.example.githubclient.views.adapters.IssueListAdapter
import com.example.githubclient.views.util.PreferencesUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_issues_list.*
import okhttp3.OkHttpClient
import org.w3c.dom.Comment
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class IssuesListFragment : Fragment() {
    private var githubRepo: GithubRepo? = null
    private var listener: IssueListEvents? = null
    private lateinit var githubAPI: GithubAPI
    private lateinit var compositeDisposable: CompositeDisposable
    private lateinit var adapterList: IssueListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            githubRepo = it.getParcelable(GITHUB_REPO_KEY)
            (context as Activity).title = githubRepo?.name
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_issues_list, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        compositeDisposable = CompositeDisposable()
        adapterList = IssueListAdapter(emptyList())
        issuesAddFab.setOnClickListener { }
        recyclerViewIssues.apply {
            adapter = adapterList
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(false)
        }
        issuesAddFab.setOnClickListener { listener?.showAddIssueDialog(githubRepo!!) }
        createGithubApi()
        getIssues()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is IssueListEvents) {
            listener = context
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun createGithubApi() {
        val username = PreferencesUtil.getString(context!!, MainActivity.USERNAME_KEY)
        val password = PreferencesUtil.getString(context!!, MainActivity.PASSWORD_KEY)
        createGithubApi(username, password)
    }


    private fun createGithubApi(username: String, password: String) {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(InterceptorApi(username, password))
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(MainActivity.ENDPOINT)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        githubAPI = retrofit.create(GithubAPI::class.java)
    }

    private fun getIssues() {
        githubRepo?.let {
            compositeDisposable.add(
                githubAPI.getIssues(it.owner.login, it.name)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSuccess(adapterList::setData)
                    .doOnError { th -> Toast.makeText(context, "Error: ${th.message}", Toast.LENGTH_SHORT).show() }
                    .subscribe()
            )
        }
    }

    fun addIssue(title: String, comment: String) {
        val issue = GithubIssue(title = title, comment = comment)
        compositeDisposable.add(
            githubAPI.postComment(githubRepo?.owner?.login!!, githubRepo?.name!!, issue)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess {
                    getIssues()
                }
                .doOnError {
                    Toast.makeText(context, "Error: ${it.message}", Toast.LENGTH_SHORT).show()
                }
                .subscribe())
    }

    internal interface IssueListEvents {
        fun showAddIssueDialog(githubRepo: GithubRepo)
    }


    companion object {

        const val GITHUB_REPO_KEY = "GITHUB_REPO_KEY"

        @JvmStatic
        fun newInstance(githubRepo: GithubRepo) =
            IssuesListFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(GITHUB_REPO_KEY, githubRepo)
                }
            }
    }
}
