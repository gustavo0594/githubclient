package com.example.githubclient.views.viewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.model.entities.GithubIssue
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cell_issues_list_layout.view.*

class IssueViewHolder (view: View) : RecyclerView.ViewHolder(view) {

    fun bind(issue: GithubIssue) {
        with(itemView) {
            issueTitleTextView.text = issue.title
            issueCommentTextView.text = issue.comment
            Picasso.get().load(issue.user?.avatarUrl).into(userIssuePicImageView)
        }
    }
}