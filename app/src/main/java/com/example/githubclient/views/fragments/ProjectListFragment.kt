package com.example.githubclient.views.fragments


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubclient.MainActivity

import com.example.githubclient.R
import com.example.githubclient.model.entities.GithubRepo
import com.example.githubclient.model.services.GithubAPI
import com.example.githubclient.model.services.InterceptorApi
import com.example.githubclient.views.adapters.RepositoryListAdapter
import com.example.githubclient.views.util.PreferencesUtil
import com.example.githubclient.views.viewHolders.RepositoryViewHolder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_project_list.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ProjectListFragment : Fragment(), RepositoryViewHolder.RepositoryListSelectedListener {

    private lateinit var githubAPI: GithubAPI
    private lateinit var compositeDisposable: CompositeDisposable
    private lateinit var adapterList: RepositoryListAdapter
    private var listener: ProjectListEvents? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_project_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        compositeDisposable = CompositeDisposable()
        adapterList = RepositoryListAdapter(emptyList(), this)
        recyclerViewRepos.apply {
            adapter = adapterList
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(false)
        }
        validateCredentials()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ProjectListEvents) {
            listener = context
        }
        (context as Activity).setTitle(R.string.repositories_title)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_credentials -> {
                listener?.showCredentialsDialog()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onRepoSelected(repo: GithubRepo) {
        listener?.showIssueListFragment(repo)
    }

    private fun validateCredentials() {
        val username = PreferencesUtil.getString(context!!, MainActivity.USERNAME_KEY, "")
        val password = PreferencesUtil.getString(context!!, MainActivity.PASSWORD_KEY, "")
        if (!username.isEmpty() && !password.isEmpty()) {
            getRepos(username, password)
        }
    }

    fun getRepos(username: String, password: String) {
        PreferencesUtil.putString(context!!, MainActivity.USERNAME_KEY, username)
        PreferencesUtil.putString(context!!, MainActivity.PASSWORD_KEY, password)
        createGithubApi(username, password)
        getRepos()
    }

    private fun createGithubApi(username: String, password: String) {
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(InterceptorApi(username, password))
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(MainActivity.ENDPOINT)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        githubAPI = retrofit.create(GithubAPI::class.java)
    }

    private fun getRepos() {
        compositeDisposable.add(
            githubAPI.getRepos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(adapterList::setData)
                .doOnError {
                    Toast.makeText(context, "Error: ${ it.message }", Toast.LENGTH_SHORT).show()
                }
                .subscribe()
        )
    }

    internal interface ProjectListEvents {
        fun showCredentialsDialog()
        fun showIssueListFragment(repo:GithubRepo)
    }

}
