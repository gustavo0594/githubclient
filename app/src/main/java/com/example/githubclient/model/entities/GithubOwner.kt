package com.example.githubclient.model.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class GithubOwner(
        val login: String,
        @SerializedName("avatar_url")
        val avatarUrl: String) : Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readString()
        )

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeString(login)
                parcel.writeString(avatarUrl)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<GithubOwner> {
                override fun createFromParcel(parcel: Parcel): GithubOwner {
                        return GithubOwner(parcel)
                }

                override fun newArray(size: Int): Array<GithubOwner?> {
                        return arrayOfNulls(size)
                }
        }
}