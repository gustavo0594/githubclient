package com.example.githubclient.model.services

import com.example.githubclient.model.entities.GithubIssue
import com.example.githubclient.model.entities.GithubRepo
import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.*

interface GithubAPI {

    @GET("user/repos?per_page=100")
    fun getRepos(): Single<List<GithubRepo>>

    @GET("/repos/{owner}/{repo}/issues")
    fun getIssues(@Path("owner") owner: String, @Path("repo") repository: String): Single<List<GithubIssue>>

    @POST("/repos/{owner}/{repo}/issues")
    fun postComment(@Path("owner") owner: String,
                    @Path("repo") repository: String,
                    @Body issue: GithubIssue): Single<ResponseBody>

}