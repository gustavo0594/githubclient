package com.example.githubclient.model.entities

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class GithubIssue(
    val id: Int = 0,
    val title: String,
    @SerializedName("comments_url")
    val commentsUrl: String? = "",
    @SerializedName("body")
    val comment: String,
    val user: GithubOwner? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readParcelable(GithubOwner::class.java.classLoader)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(commentsUrl)
        parcel.writeString(comment)
        parcel.writeParcelable(user, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GithubIssue> {
        override fun createFromParcel(parcel: Parcel): GithubIssue {
            return GithubIssue(parcel)
        }

        override fun newArray(size: Int): Array<GithubIssue?> {
            return arrayOfNulls(size)
        }
    }
}