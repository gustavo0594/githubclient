package com.example.githubclient.model.services

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response

class InterceptorApi(private val username: String, private val password: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val builder = request.newBuilder().header("Authorization", Credentials.basic(username, password))
        return chain.proceed(builder.build())
    }
}